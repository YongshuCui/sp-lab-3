
//open the file
FILE *open_file(char *fname){
    if(fopen(fname,"r") == NULL){
        printf("Fail to open");
        exit(EXIT_FAILURE);
    }
    return fopen(fname,"r");
}

//the function to get mean
double get_mean(float *p, int n){
    double sum = 0;
    for(int i = 0;i<n;i++)
        sum = sum+p[i];
    double mean = sum/n;
    return mean;

}

//the function to get median
double get_median(float *cur_arr ,int n){
    if (n%2 == 1)
        return (cur_arr[n/2]);
    else{
        double min = cur_arr[n/2 -1];
        double max = cur_arr[n/2];
        return (min+max)/2;
    }

}

//the function to get standard deviation
double get_stddev(float *p ,int n){
    //sqrt ((sum ((xi - mean)^2))/N);
    double stddev = 0;
    double mean = get_mean(p, n);
    for (int i =0; i < n; i++) {
        stddev += pow(p[i] - mean, 2);
    }
    return sqrt(stddev/n);
}

//the function to sort the array
void swap(float *xp, float *yp){
    float temp = *xp;
    *xp = *yp;
    *yp = temp;

}


float sort(float *cur_arr,int n){
    int i, j, min_idx;
    // move the boundary of unsorted subarray one by one
    for (i=0; i < n-1; i++) {
        //Search the mim element in this unsorted array
        min_idx = i;
        for(j = i+1; j < n; j++)
            if (cur_arr[j] < cur_arr[min_idx])
                min_idx = j;
        //Sawp the found min element with the first element
        swap(&cur_arr[min_idx], &cur_arr[i]);

    }
    return 0;
}

