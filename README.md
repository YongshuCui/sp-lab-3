## Basicstats

## Build
Clone the code from the BitBucket.
BitBucket Link: git clone https://YongshuCui@bitbucket.org/YongshuCui/sp-lab-3.git
Build and run.

The system implements basic statistical functions in C programming.

- Firstly, the C program needs to open and read the file.
- Secondly,  defines the mean, median, and standard deviation of the array.
- Last, sort this array and print the mean,median and the standard deviation.


In  this Version, This project implements four major functions:
- Open the file
- Fuction to get mean, median, standard deviation
- Sort the element of the array
- Print the array 


## Step to run
1. Open the program directory, The project contains the following file structure:


├── basicstats.c                // main program
├── readfile.h		         // library about read file,
├── README.md	         // program documentation
├── small.txt                      // db file
├── large.txt                       // db file
└── a.out		                 // execute program

Please input  if you execute the program
./a.out small.txt


