//Yongshu_Cui
// 11/18/2020

#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "readfile.h"
//
////open the file
//FILE *open_file(char *fname){
//    if(fopen(fname,"r") == NULL){
//        printf("Fail to open");
//        exit(EXIT_FAILURE);
//    }
//    return fopen(fname,"r");
//}
//
////the function to get mean
//double get_mean(float *p, int n){
//    double sum = 0;
//    for(int i = 0;i<n;i++)
//        sum = sum+p[i];
//    double mean = sum/n;
//    return mean;
//
//}
//
////the function to get median
//double get_median(float *cur_arr ,int n){
//    if (n%2 == 1)
//        return (cur_arr[n/2]);
//    else{
//        double min = cur_arr[n/2 -1];
//        double max = cur_arr[n/2];
//        return (min+max)/2;
//    }
//
//}
//
////the function to get standard deviation
//double get_stddev(float *p ,int n){
//    //sqrt ((sum ((xi - mean)^2))/N);
//    double stddev = 0;
//    double mean = get_mean(p, n);
//    for (int i =0; i < n; i++) {
//        stddev += pow(p[i] - mean, 2);
//    }
//    return sqrt(stddev/n);
//}
//
////the function to sort the array
//void swap(float *xp, float *yp){
//    float temp = *xp;
//    *xp = *yp;
//    *yp = temp;
//
//}
//
//
//float sort(float *cur_arr,int n){
//    int i, j, min_idx;
//    // move the boundary of unsorted subarray one by one
//    for (i=0; i < n-1; i++) {
//        //Search the mim element in this unsorted array
//        min_idx = i;
//        for(j = i+1; j < n; j++)
//            if (cur_arr[j] < cur_arr[min_idx])
//                min_idx = j;
//        //Sawp the found min element with the first element
//        swap(&cur_arr[min_idx], &cur_arr[i]);
//
//    }
//    return 0;
//}


int main(int argc, char *argv[]){
    if (argc < 2) {
        printf("Please input the filename to read...\n ");
        return 0;
    }


    int s = 20, length_arr = 0;
//float temp;
    float *cur_arr = (float *) malloc(s * sizeof(float));
    char *fname = argv[1];
    FILE *fp = open_file(fname);

    while (!feof(fp)){
        fscanf(fp,"%f\n",(cur_arr+length_arr));
        length_arr++;
        if(length_arr == s){
            //The current length of array is equal to max capacity
            //Create new array with twice size
            float *new_arr = (float *) malloc(s * 2 * sizeof(float));
            //Copy values from the old temp array to the new
            memcpy(new_arr, cur_arr, length_arr * sizeof(float));
            //free the memory
            free(cur_arr);
            cur_arr = new_arr;
            //To increase the size of n
            s = s *2;

        }
    }

//To close the file
    fclose(fp);
    double mean = get_mean(cur_arr, length_arr);
    double stddev = get_stddev(cur_arr, length_arr);
    printf( "Num values          : %5d\n", length_arr);
//    double mean = get_mean(cur_arr, length_arr);
//    double stddev = get_stddev(cur_arr, length_arr);
    printf("The mean is          : %.3f\n", mean);
    printf("The stddev is        : %.3f\n", stddev);
    sort(cur_arr, length_arr);
    double median = get_median(cur_arr, length_arr);
    printf("The median is        : %.3f\n", median);
    printf("Unused array capacity: %5d\n", s-length_arr);

}

